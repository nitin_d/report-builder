import { Component, ViewChild, ComponentFactoryResolver, ViewContainerRef } from '@angular/core';
import { RowComponent } from './row-layout/row-layout.component';
import { ColumnComponent } from './column-layout/column-layout.components';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  showModal;
  @ViewChild('reportContainer', { read: ViewContainerRef }) entry: ViewContainerRef;

  constructor(private resolver: ComponentFactoryResolver) { 
    
  }
  
  /**
   * Open the first wizard by setting a boolean varaible 
  */
  openWizard(){
    this.showModal = true;
  }

  /**
   * Create the layout based on teh users  selection for rows and columns
   * Close the wizard
   */
  createLayout(evt){
    this.showModal = false;
    this.createConatinerLayout(evt);
  }

  /**
   * 
   * @param rowMappings  - Contains a map for all rows and columns in each row
   * Iterate thru the map and create the layout 
   * Add a subscriber to open the visualization wizard when an individual container is clicked
   */
  createConatinerLayout(rowMappings){
    this.entry.clear();
      for(let item in rowMappings){
        const rowfactory = this.resolver.resolveComponentFactory(RowComponent);
        let rowComponentRef = this.entry.createComponent(rowfactory);
        rowComponentRef.instance.entry.clear();
        rowComponentRef.instance.dynamicId = "row_" + item;
        for(let i=0; i< (+rowMappings[item]); i++){
          const colfactory = rowComponentRef.instance.resolver.resolveComponentFactory(ColumnComponent);
          let colComponentRef = rowComponentRef.instance.entry.createComponent(colfactory);
          colComponentRef.instance.dynamicId = "reporContainer_" + rowComponentRef.instance.dynamicId + "_col_" + i;
          this.addSubscriber(colComponentRef.instance);
        }
      }
  }

  addSubscriber(componentRef){
    componentRef.createVisualizationWizard.subscribe(res => {
      this.showHideVisualizationModal(res);
    })
  }
  ////////////////////////////////////////
  showVisualizationModal;
  displayVisualizationSteps;
  selectedContainerRef;

  showHideVisualizationModal(res : any){
    this.displayVisualizationSteps = 3;
    this.selectedContainerRef = res.containerRef;
    this.showVisualizationModal = true;
  }
}
