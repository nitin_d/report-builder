import { Component, Input, Output, EventEmitter, ViewChild, ViewContainerRef } from '@angular/core';

@Component({
    selector: 'column-component',
    templateUrl: './column-layout.components.html',
    styleUrls: ['./column-layout.components.css']
})
export class ColumnComponent {
    /**
     * Indicates the unique ID generated for each component
     */
    @Input()
    public dynamicId;

    /**
     * Emitter to wmit the evnt for creating visualization
     */
    @Output()
    createVisualizationWizard : EventEmitter<any> = new EventEmitter();

    /**
     * Reference to teh template
     */
    @ViewChild('visualizationContainer', { read: ViewContainerRef }) entry: ViewContainerRef;

    /**
     * Method to emit the event to start creating visualization
     */
    onCreateVisualization(){
        this.createVisualizationWizard.emit({"show" : true , "conatinerId" : this.dynamicId, "containerRef" : this.entry});
    }
}