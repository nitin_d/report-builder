import { Component, OnInit } from '@angular/core';
import { Chart } from 'angular-highcharts';

@Component({
    selector: 'pie-chart-component',
    templateUrl: './pie-chart.component.html',
    styleUrls: ['./pie-chart.component.css']
})
export class PieChartComponent implements OnInit {
    chart = new Chart({
        chart: {
            type: 'line'
        },
        title: {
            text: 'Linechart'
        },
        credits: {
            enabled: false
        },
        series: [
            {
                name: 'Line 1',
                data: [1, 2, 3]
            }
        ]
    });
    constructor() {

    }

    ngOnInit() {
        this.chart.addPoint(Math.floor(Math.random() * 10));
    }
}