import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { IVisualization } from '../interfaces/visualization-item';
import { VisualizationService } from '../services/visualization.service';
import { VisualizationItem } from '../model/visualization-item';

@Component({
    selector: 'visualization-selector-component',
    templateUrl: './visualization-selector.component.html',
    styleUrls: ['./visualization-selector.component.css']
})
export class VisualizationSelectorComponent implements OnInit{
    visualizationArray : any;

    /**
     * Method to create the selected chart
     */
    @Output()
    createSelectedChartEvent : EventEmitter<any> = new EventEmitter();

    constructor(private visualizationService : VisualizationService) { 
        
    } 

    /**
     * setup teh master list of visualization array
     */
    public ngOnInit(){
        this.visualizationArray = this.visualizationService.getVisualizations();
    }

    /**
     * Method to emit teh event to create the visualization
     */
    onVisualizationClicked(item){
        this.createSelectedChartEvent.emit(item.data);
    }
}