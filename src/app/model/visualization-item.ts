import { Type } from '@angular/core';

export class VisualizationItem {
  constructor(public component: Type<any>, public data: any) {}
}
