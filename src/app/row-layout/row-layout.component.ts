import { Component, Input, ViewChild, ViewContainerRef, ComponentFactoryResolver } from '@angular/core';

@Component({
    selector: 'row-component',
    templateUrl: './row-layout.component.html',
    styleUrls: ['./row-layout.component.css']
})
export class RowComponent {
    totalRows;

    /**
     * Rference to teh template
     */
    @ViewChild('colContainer', { read: ViewContainerRef }) entry: ViewContainerRef;

    /**
     * Indicates the unique ID generated for each component
     */
    @Input()
    public dynamicId;

    constructor(public resolver: ComponentFactoryResolver) { 
        
    }
}