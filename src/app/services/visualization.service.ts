import { Injectable }           from '@angular/core';

import { PieChartComponent } from '../visualization-components/pie-chart/pie-chart.component';
import { DimensionChartComponent } from '../visualization-components/dimension-chart/dimension-chart.component';
import { VisualizationItem } from '../model/visualization-item';

@Injectable()
export class VisualizationService {
  getVisualizations() {
    return [
      new VisualizationItem(PieChartComponent, {name: 'Pie', component : PieChartComponent}),

      new VisualizationItem(DimensionChartComponent, {name: 'Bar', component : DimensionChartComponent})
    ];
  }
}


