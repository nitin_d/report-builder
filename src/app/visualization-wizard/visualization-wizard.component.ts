import { Component, Input, ViewChild, ElementRef, Output, EventEmitter, ViewContainerRef, ComponentFactoryResolver } from '@angular/core';
import { VisualizationSelectorComponent } from '../visualization-selector/visualization-selector.component';
import { IVisualization } from '../interfaces/visualization-item';
import { PieChartComponent } from '../visualization-components/pie-chart/pie-chart.component';

@Component({
    selector: 'visualization-wizard',
    templateUrl: './visualization-wizard.component.html',
    styleUrls: ['./visualization-wizard.component.css']
})
export class VisualizationWizardComponent {
    _showModal;

    /**
     * Reference to the template
     */
    @ViewChild('visualizationContainer', { read: ViewContainerRef }) entry: ViewContainerRef;

    /**
     * Variable to hold the current wizard step number
     */
    displayStep;

    /**
     * Total steps in the wizard
     */
    @Input()
    totalSteps = 2;

    /**
     * Variable to hold refeerence to the current selected container
     */
    @Input()
    selectedContainerRef;

    /**
     * Variable to Hide/Show Next button
     */
    showNext;

    /**
     * Conatins the component class name for each step of the wiard
     */
    displayArray = ["VisualizationSelectorComponent", "ChartPlacehoder"];

    constructor(private resolver: ComponentFactoryResolver) { 
    
    }

    
    /**
     * setter to control Show/Hide Modal
     */
    @Input()
    set showModal(value: boolean) {
        this._showModal = value;
        this.displayStep = 1;
        this.showNext = true;
        this.entry.clear();
        this.initDisplayStep();
    }

    get showModal() {
        return this._showModal;
    }

    onSubmit() {
    }

    /**
     * Start the next step of the wizard
     * Optional paramter indicates if the step needs data from previous step
     */
    next(itemData?){
        if(this.displayStep + 1 == this.totalSteps){
            this.showNext = false;
        }
        this.displayStep++;
        this.initDisplayStep(itemData);
    }

    /**
     * Shows the componnet in teh dynamic template based on teh step number
     */
    initDisplayStep(itemData?){
        let currentStepItem = this.displayArray[this.displayStep - 1];
        
        switch(currentStepItem){
            case "VisualizationSelectorComponent" : 
                let compInstance : any = this.loadComponent(VisualizationSelectorComponent);
                compInstance.createSelectedChartEvent.subscribe(chartItem => {
                    this.createChart(chartItem);
                })
                break;
            case "ChartPlacehoder":
                this.createSelectedChart(itemData);
                break;
        }
    }
    /**
     * Load the component
     */
    loadComponent(comp){
        const compfactory = this.resolver.resolveComponentFactory(comp);
        let visualizationComponentRef : any = this.entry.createComponent(compfactory);
        return visualizationComponentRef.instance;
    }

    createChart(chartItem){
        this.next(chartItem);

    }

    createSelectedChart(itemData : any){
        this.entry.clear();
        switch(itemData.name){
            case "Pie" : 
            this.selectedContainerRef.clear();
            const compfactory = this.resolver.resolveComponentFactory(itemData.component);
            let visualizationComponentRef : any = this.selectedContainerRef.createComponent(compfactory);
            this.showModal = false;
        }
    }
}
