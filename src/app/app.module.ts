import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ChartModule } from 'angular-highcharts';

import { AppComponent } from './app.component';
import { WizardComponent } from './wizard/wizard.component';
import { RowComponent } from './row-layout/row-layout.component';
import { ColumnComponent } from './column-layout/column-layout.components';
import { VisualizationWizardComponent } from './visualization-wizard/visualization-wizard.component';
import { PieChartComponent } from './visualization-components/pie-chart/pie-chart.component';
import { DimensionChartComponent } from './visualization-components/dimension-chart/dimension-chart.component';
import { VisualizationSelectorComponent } from './visualization-selector/visualization-selector.component';
import { VisualizationService } from './services/visualization.service';

@NgModule({
  declarations: [
    AppComponent,
    WizardComponent,
    RowComponent,
    ColumnComponent,
    VisualizationWizardComponent,
    PieChartComponent,
    DimensionChartComponent,
    VisualizationSelectorComponent,
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ChartModule
  ],
  entryComponents: [
    RowComponent,
    ColumnComponent,
    VisualizationSelectorComponent,
    PieChartComponent,
    DimensionChartComponent
  ],
  providers: [VisualizationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
