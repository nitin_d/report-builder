import { Component, Input, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'wizard',
    templateUrl: './wizard.component.html',
    styleUrls: ['./wizard.component.css']
})
export class WizardComponent {
    _showModal;
    @ViewChild('wizardPopup') wizardPopupRef: ElementRef;

    @Output()
    changeShowWizard : EventEmitter<any> = new EventEmitter();

    @Output()
    createLayoutEmitter : EventEmitter<any> = new EventEmitter();
    
    displayStep;
    totalSteps = 2;
    showNext;

    @Input()
    set showModal(value: boolean) {
        this._showModal = value;
        this.displayStep = 1;
        this.showNext = true;
    }

    get showModal() {
        return this._showModal;
    }

    onSubmit() {
        this.createLayoutEmitter.emit(this.rowColumnMapping);
    }

    next(){
        if(this.displayStep + 1 == this.totalSteps){
            this.showNext = false;
        }
        this.displayStep++;
        this.initSteps();
    }

    initSteps(){
        switch(this.displayStep){
                case 2 :
                    for(let i = 0; i < this.totalRows; i++){
                        this.rowColumnMapping[i] = "1";
                    }
                    break;
        } 
    }

    /////////////////////////////////
    totalRows = 1;
    rowColumnMapping : any = {};

    onInptChange(evt, rowIndex){
        this.rowColumnMapping[rowIndex] = evt.currentTarget.value;
    }
}
